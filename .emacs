
;; This is a fix for issues with Emacs 26.1.  This is supposedly fixed
;; in 26.3, so future versions shouldn't require setting this
;; variable.
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")


(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)

