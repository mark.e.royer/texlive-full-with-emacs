FROM debian:bookworm-slim
MAINTAINER Mark Royer <mark.royer@maine.edu>

ENV TERM dumb

# Install the base system, texlive and other items
COPY install-packages.sh .
RUN ./install-packages.sh && rm install-packages.sh

# Create the emacs user
RUN adduser --disabled-password --gecos "" emacs

# Setup emacs stuff
RUN mkdir -p /home/emacs
WORKDIR /home/emacs
ENV HOME /home/emacs

USER emacs
COPY .emacs .
COPY install-emacs-packages.el .
COPY install-emacs-items.sh .
RUN ./install-emacs-items.sh && rm install-emacs-items.sh

# Texlive cache directory
RUN mkdir -p /home/emacs/.texlive/texmf-var
ENV TEXMFVAR /home/emacs/.texlive/texmf-var

# Ensure that the font database is built
RUN luaotfload-tool --update

# Verify TEXMFVAR
RUN echo "TEXMFVAR is: $TEXMFVAR"

# Verify directory contents.
RUN ls -al $TEXMFVAR

USER root
# Allow users to have read write permissions on the emacs directory
RUN find /home/emacs -type d -exec chmod ugo+rwx {} + &&\
    find /home/emacs -type f -exec chmod ugo+rw {} + 

# Allow users to add pygments lexers and styles

RUN chmod ugo+rw /usr/lib/python3/dist-packages/pygments/lexers &&\
    chmod ugo+rw /usr/lib/python3/dist-packages/pygments/styles &&\
    chmod ugo+wr /usr/lib/python3/dist-packages/pygments/lexers/_mapping.py
