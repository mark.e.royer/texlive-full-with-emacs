dockertestimage :=texlive-full-with-emacs:test

dockeropts :=-u `id -u $(USER)`:`id -g $(USER)` -w /test \
-v $(PWD):/test \
-it --rm

.PHONY: clean make-test run-test

run-test: make-test
	docker run $(dockeropts) $(dockertestimage) bash

make-test:
	docker build -t $(dockertestimage) .

clean:
	docker image rm $(dockertestimage)
