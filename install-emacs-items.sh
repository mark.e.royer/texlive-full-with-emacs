#!/usr/bin/env bash
#
# Set up personal Emacs packages and files.

# Install packages
echo "Installing requested Emacs packages."
emacs --script install-emacs-packages.el

# org-reveal customization
echo "(require 'ox-reveal)" >> .emacs
echo '(setq org-reveal-root "https://cdn.jsdelivr.net/npm/reveal.js")' >> .emacs

# Clean up and make all items read, write, and accessible for any user
rm install-emacs-packages.el
chmod ugo+rw .emacs
find .emacs.d -type d -exec chmod ugo+rwx {} +
find .emacs.d -type f -exec chmod ugo+rw {} + 

echo "Finished with all Emacs items."
