;;; install-emacs-packages.el --- Install the required emacs packages.

;;; Commentary:
;;;
;;; Execute this script by typing
;;;
;;; =emacs --script install-emacs-packages.el=
;; 

;;; Code:

(load "/home/emacs/.emacs")

(package-refresh-contents)

;; Add packages you want to install here.

(package-install 'ox-reveal)


(provide 'install-emacs-packages)
;;; install-emacs-packages.el ends here
